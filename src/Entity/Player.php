<?php

namespace App\Entity;

use App\Repository\PlayerRepository;
use Doctrine\ORM\Mapping as ORM;
use stdClass;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;

#[ORM\Entity(repositoryClass: PlayerRepository::class)]
class Player
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'players')]
    #[ORM\JoinColumn(nullable: false)]
    private $user;

    #[ORM\Column(type: 'json')]
    private $grid = [];

    #[ORM\Column(type: 'json')]
    private $boats = [];

    #[ORM\ManyToOne(targetEntity: Room::class, inversedBy: 'players')]
    #[ORM\JoinColumn(nullable: false)]
    private $room;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getGrid(): ?array
    {
        return $this->grid;
    }

    public function setGrid(array $grid): self
    {
        $tempgrid = array();

        foreach($grid as $index => $cell)
        {
            $tempgrid[] = $this->cast($cell, Cell::class);
        }

        $this->grid = $tempgrid;

        return $this;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function setRoom(?Room $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function getBoats(): ?array
    {
        return $this->boats;
    }

    public function setBoats(array $boats): self
    {
        $this->boats = $boats;

        return $this;
    }

    public function placeBoat(Boat $boat, int $x, int $y) : bool
    {
        for ($xi = 0; $xi < $boat->getSize()[0] - 1; $xi++);
        {
            for ($yi = 0; $yi < $boat->getSize()[1] - 1; $yi++);
            {
                /*
                 * if the clicked cell is a near the right or bottom side and the boat would normally be placed outside the grid
                 * (Avoiding an Array out of range Exception)
                 */
                if ($x + $xi > count($this->grid["rows"]) || $y + $yi > count($this->grid["rows"][0]))
                {
                    return false;
                }

                $cell = $this->grid["rows"][$x + $xi][$y + $yi];

                if (!$cell["isPlayable"] || $cell["isOccupied"]) {
                    return false;
                }
            }
        }

        // TODO: Modify the cell's content here (issue linking a cell to a boat part)


        return true;
    }

    protected function cast(stdClass $instance, string $className): mixed
    {
        return unserialize(sprintf(
            'O:%d:"%s"%s',
            \strlen($className),
            $className,
            strstr(strstr(serialize($instance), '"'), ':')
        ));
    }
    public function attack(Boat $boat, bool $hit, int $x, int $y, int $size ) : bool
    {
       /* if ($boat != null) = true

            if ($hit =true);
              Boat-> $Size - 1

              if ($Size = 0)


            if ($hit = false)*/





    }
}
