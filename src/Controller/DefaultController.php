<?php

namespace App\Controller;

use App\Entity\Boat;
use App\Entity\Player;
use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use stdClass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\Room;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'homepage')]
    public function index(): Response
    {
        return $this->render('default/index.html.twig', [
            "id" => 1
        ]);
    }

    #[Route('/play', name: 'play')]
    public function play(ManagerRegistry $doctrine): Response
    {
        $repo = $doctrine->getManager()->getRepository(Room::class);

        $qb = $repo->createQueryBuilder('r');

        $rooms = $qb->select('r')
            ->where('remaining_slots >= 1')
            ->andWhere('state = 0')
            ->getQuery()
            ->execute();

        if (count($rooms))
        {
            return $this->redirectToRoute("/play/{$rooms[0]->getId()}");
        }
        else
        {
            $room = new Room();
            $room->setState(0);

            $doctrine->getManager()->persist($room);
            $doctrine->getManager()->flush();

            return $this->redirectToRoute("/play/{$rooms[0]->getId()}");
        }


    }

    #[Route('/play/{id}', name: 'playId')]
    public function playId(int $id): Response
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @route("/update/room/{id}", name="game", methods={"POST"})
     *
     * @IsGranted("ROLE_USER", statusCode=403, message="Why would you send a request to a room you're not part of, Anno")
     */
    public function game(HubInterface $hub, ManagerRegistry $doctrine, Request $request, int $id): Response
    {
        $user = $this->getUser();

        $repo = $doctrine->getManager()->getRepository(Room::class);

        $room = $repo->find($id);

        if (!$this->isUserInRoom($doctrine, $room, $user)) {
            return new Response(
                json_encode(
                    array(
                        "result" => "error",
                        "message" => "Why would you send a request to a room you're not part of, {$user->getUserIdentifier()} ?"
                    )
                )
            );
        }
        $player = $room->getPlayers()[0]->getUser()->getId() == $user->getId() ? $room->getPlayers()[0] : $room->getPlayers()[1];

        $data =  json_decode($request->request->get("data"));

        $action = $data->body->action ?? "";

        // Process request and game in another, separate function
        $updateData = $this->ProcessGame($room, $player, $data->body);

        // TODO: Only parts that changed should be in the returned data
        if ($action == "attack")
        {
            $update = new Update("/update/room/{$id}",
                json_encode(
                    array(
                        "result" => "success",
                        "data" => $updateData
                    )
                )
            );

            $hub->publish($update);
        }
        else
        {
            return new Response(json_encode($updateData));
        }

        return new Response("Success");
    }

    /**
     * Une fonction pour tester mercure, utilisé lors du setup de mercure
     *
     * @Route("/update/test/room/{id}", name="test")
     *
     * @IsGranted("ROLE_USER", statusCode=403, message="Why would you send a request to a room you're not part of, Anno")
     */
    public function test(ManagerRegistry $doctrine, HubInterface $hub, ?int $id): Response
    {
        $user = $this->getUser();

        $repo = $doctrine->getManager()->getRepository(Room::class);

        $room = $repo->find($id);

        if (!$this->isUserInRoom($doctrine, $room, $user))
        {
            return new Response("Failure");
        }

        $player = $room->getPlayers()[0]->getUser()->getId() == $user->getId() ? $room->getPlayers()[0] : $room->getPlayers()[1];

        $boat = new Boat();

        $boat->setSize([2, 1]);

        $update = new Update(sprintf("/update/test/%s", $id),
            json_encode(
                array(
                    "result" => "success",
                    "data" => sprintf("boat %s be placed", $player->placeBoat($boat, 10, 10) ? "can" : "cannot")
                )
            )
        );

        $hub->publish($update);

        return new Response("Success");
    }

    public function ProcessGame(Room $room, Player $player, stdClass $data): array
    {
        switch($data->action)
        {
            case "place":
                if ($room->getState() != 1)
                    return array(
                        "result" => "error",
                        "message" => "Current game isn't in placement period."
                    );

                if (isset($data->id))
                    $boat = $this->getBoat($player, $data->id);
                else
                    return array(
                        "result" => "error",
                        "message" => "no boat ID provided"
                    );

                if ($boat == null)
                    return array(
                        "result" => "error",
                        "message" => sprintf("No boat with id %d", $data->id)
                    );

                if (isset($action->position) && $action->position instanceof stdClass)
                {
                    if (isset($action->position->x) && isset($action->position->y))
                    {
                        if (is_int($action->position->x) && is_int($action->position->y))
                        {
                            if ($player->placeBoat($boat, $action->position->x, $action->position->y))
                            {
                                $boat->setPosition($action->position->x, $action->position->y);
                            }
                            else
                            {
                                return array(
                                    "result" => "error",
                                    "message" => sprintf("%s (id = %d) cannot be placed at (%d;%y)",
                                        $boat->getName(),
                                        $boat->getId(),
                                        $action->position->x,
                                        $action->position->y
                                    )
                                );
                            }
                        }
                    }
                }

                return array(
                    "result" => "success"
                );

            case "attack":
                //room si les id sont les memes on récupère le premier joueur sinon c'est le second
                $player = $room->getPlayers()[0]->getUser()->getId() == $this->getUser()->getId() ? $room->getPlayers()[1] : $room->getPlayers()[0];
                $turn = $room->getTurn();
                $cell = $player -> getGrid() [$data->position->x][$data->position->y];
                if($cell->hit == false ) {
                    if ($cell->boat == null) {

                        $room->setTurn($turn == 0 ? 1 : 0);
                        return array(
                            "result" => "missed",
                            "position" => array(
                                "X" => $data->position->x,
                                "Y" => $data->position->y,
                            )
                        );

                    } else {
                        $cell->hit = true;
                        $cell->boat->health--;
                        if ($cell->boat->health == 0) {
                            return array(
                                "result" => "drowned",
                                "position" => array(
                                    "X" => $data->position->x,
                                    "Y" => $data->position->y,
                                ));
                        }
                        return array(
                            "result" => "hit",
                            "position" => array(
                                "X" => $data->position->x,
                                "Y" => $data->position->y,
                            )
                        );

                    }
                }
                else {
                    return array(
                    "result" => "error",
                    "message"=>"%s (id = %d) cannot be placed at (%d;%y)",
                        );

                }


                break;

            default:
                return array(
                    "result" => "error",
                    "message" => "action is invalid or not provided."
                );
        }

        return array(
            "result" => "Unknown",
            "message" => "Is this a cosmic ray?"
        );
    }

    public function getBoat(Player $player, int $id): ?Boat
    {
        if ($id < count($player->getBoats()))
            return $this->cast($player->getBoats()[$id], Boat::class);

        return null;
    }

    public function isUserInRoom(ManagerRegistry $doctrine, Room $room, User $user): bool
    {
        foreach ($room->getPlayers() as $player)
        {
            if ($player->getUser()->getId() == $user->getId())
            {
                return true;
            }
        }

        return false;
    }

    protected function cast(StdClass $instance, string $className): mixed
    {
        return unserialize(sprintf(
            'O:%d:"%s"%s',
            \strlen($className),
            $className,
            strstr(strstr(serialize($instance), '"'), ':')
        ));
    }
}
